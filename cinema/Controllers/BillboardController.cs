﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cinema.Models;

namespace cinema.Controllers
{   
    public class BillboardController : Controller
    {
        // GET: Schedule
        public ActionResult Index()
        {
            ApplicationDbContext ac = new ApplicationDbContext();           
            return View(ac.Films);
        }

        public ActionResult Film(int id) {
            ApplicationDbContext ac = new ApplicationDbContext();
            var film = ac.Films.Find(id);
            return View(film);
        }

    }
}