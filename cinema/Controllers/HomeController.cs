﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using cinema.Models;
using System.Linq;
using System.Data.Entity;

namespace cinema.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //return View();
            return View(new object[] { (new CinemaServiceImpl()).findAll(), (new TagServiceImpl()).findAllGenre(), (new AgeLimitServiceImpl()).FindAll() });
        }

        [HttpGet]
        public ActionResult Timetable(DateTime date, int cinema = 0, int tag = 0, int limit = 0)
        {
            //return View("_TimeTable");
            return View("_TimeTable", new FilmServiceImpl().findFilterAll(startDay: date, cinemaId: cinema, tagId: tag, limit: limit));
        }

        [HttpGet]
        public ActionResult Film(int id)
        {
            FilmServiceImpl service = new FilmServiceImpl();
            return View(new object[] { service.getFilmInfo(id), service.getFilmRaiting(id), service.getFilmActors(id), service.getFilmProducer(id), service.getFilmSchedule(new DateTime(), 0, id), service.getFilmReviews(id, 3) });
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}