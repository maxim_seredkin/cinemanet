namespace cinema.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Second : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Resources", "ResourceName", c => c.String(nullable: false));
            DropColumn("dbo.Resources", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Resources", "Name", c => c.String(nullable: false));
            DropColumn("dbo.Resources", "ResourceName");
        }
    }
}
