namespace cinema.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActiveOpenTickets",
                c => new
                    {
                        ActiveOpenTicketID = c.Int(nullable: false),
                        OpenTicketID = c.Int(nullable: false),
                        TicketID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ActiveOpenTicketID)
                .ForeignKey("dbo.OpenTickets", t => t.ActiveOpenTicketID)
                .ForeignKey("dbo.Tickets", t => t.ActiveOpenTicketID)
                .Index(t => t.ActiveOpenTicketID);
            
            CreateTable(
                "dbo.OpenTickets",
                c => new
                    {
                        OpenTicketID = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.String(nullable: false, maxLength: 128),
                        CodeTicket = c.String(nullable: false),
                        BuyDate = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OpenTicketID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateBorn = c.DateTime(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ReserveTickets",
                c => new
                    {
                        ReserveTicketID = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.String(nullable: false, maxLength: 128),
                        PlaceScheduleID = c.Int(nullable: false),
                        CodeTicket = c.String(nullable: false),
                        ReserveDate = c.DateTime(nullable: false),
                        EndReserveDate = c.DateTime(nullable: false),
                        IsConfirmed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ReserveTicketID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .ForeignKey("dbo.PlaceSchedules", t => t.PlaceScheduleID)
                .Index(t => t.ApplicationUserID)
                .Index(t => t.PlaceScheduleID);
            
            CreateTable(
                "dbo.PlaceSchedules",
                c => new
                    {
                        PlaceScheduleID = c.Int(nullable: false, identity: true),
                        ScheduleID = c.Int(nullable: false),
                        Row = c.Int(nullable: false),
                        Place = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PlaceScheduleID)
                .ForeignKey("dbo.Schedules", t => t.ScheduleID)
                .Index(t => t.ScheduleID);
            
            CreateTable(
                "dbo.Schedules",
                c => new
                    {
                        ScheduleID = c.Int(nullable: false, identity: true),
                        FilmID = c.Int(nullable: false),
                        HallID = c.Int(nullable: false),
                        ScheduleDate = c.DateTime(nullable: false),
                        TicketCost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Is3D = c.Boolean(),
                    })
                .PrimaryKey(t => t.ScheduleID)
                .ForeignKey("dbo.Films", t => t.FilmID)
                .ForeignKey("dbo.Halls", t => t.HallID)
                .Index(t => t.FilmID)
                .Index(t => t.HallID);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        FilmID = c.Int(nullable: false, identity: true),
                        FilmName = c.String(),
                        Country = c.String(),
                        Length = c.Time(nullable: false, precision: 7),
                        ReleaseDate = c.DateTime(nullable: false),
                        Finance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UrlTrailer = c.String(),
                        Description = c.String(),
                        AgeLimitID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FilmID)
                .ForeignKey("dbo.AgeLimits", t => t.AgeLimitID)
                .Index(t => t.AgeLimitID);
            
            CreateTable(
                "dbo.AgeLimits",
                c => new
                    {
                        AgeLimitID = c.Int(nullable: false, identity: true),
                        Age = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.AgeLimitID);
            
            CreateTable(
                "dbo.ImageFilms",
                c => new
                    {
                        ImageFilmID = c.Int(nullable: false, identity: true),
                        FilmID = c.Int(nullable: false),
                        IsPoster = c.Boolean(nullable: false),
                        FileName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ImageFilmID)
                .ForeignKey("dbo.Films", t => t.FilmID)
                .Index(t => t.FilmID);
            
            CreateTable(
                "dbo.Raitings",
                c => new
                    {
                        RaitingID = c.Int(nullable: false, identity: true),
                        ResourceID = c.Int(nullable: false),
                        FilmID = c.Int(nullable: false),
                        Value = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.RaitingID)
                .ForeignKey("dbo.Films", t => t.FilmID)
                .ForeignKey("dbo.Resources", t => t.ResourceID)
                .Index(t => t.ResourceID)
                .Index(t => t.FilmID);
            
            CreateTable(
                "dbo.Resources",
                c => new
                    {
                        ResourceID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Url = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ResourceID);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        ReviewID = c.Int(nullable: false, identity: true),
                        FilmID = c.Int(nullable: false),
                        IsLike = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Title = c.String(nullable: false),
                        Description = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ReviewID)
                .ForeignKey("dbo.Films", t => t.FilmID)
                .Index(t => t.FilmID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        TagID = c.Int(nullable: false, identity: true),
                        TypeTagID = c.Int(nullable: false),
                        TagName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.TagID)
                .ForeignKey("dbo.TypeTags", t => t.TypeTagID)
                .Index(t => t.TypeTagID);
            
            CreateTable(
                "dbo.TypeTags",
                c => new
                    {
                        TypeTagID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TypeTagID);
            
            CreateTable(
                "dbo.Halls",
                c => new
                    {
                        HallID = c.Int(nullable: false, identity: true),
                        HallName = c.String(nullable: false),
                        Row = c.Int(nullable: false),
                        Place = c.Int(nullable: false),
                        ScreenWidth = c.Int(),
                        ScreenHeigth = c.Int(),
                        CinemaID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HallID)
                .ForeignKey("dbo.Cinemas", t => t.CinemaID)
                .Index(t => t.CinemaID);
            
            CreateTable(
                "dbo.Cinemas",
                c => new
                    {
                        CinemaID = c.Int(nullable: false, identity: true),
                        Address = c.String(nullable: false),
                        CinemaName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CinemaID);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketID = c.Int(nullable: false, identity: true),
                        ApplicationUserID = c.String(nullable: false, maxLength: 128),
                        PlaceScheduleID = c.Int(nullable: false),
                        CodeTicket = c.String(nullable: false),
                        BuyDate = c.DateTime(nullable: false),
                        IsOpen = c.Boolean(nullable: false),
                        IsReserve = c.Boolean(nullable: false),
                        Schedule_ScheduleID = c.Int(),
                    })
                .PrimaryKey(t => t.TicketID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .ForeignKey("dbo.PlaceSchedules", t => t.PlaceScheduleID)
                .ForeignKey("dbo.Schedules", t => t.Schedule_ScheduleID)
                .Index(t => t.ApplicationUserID)
                .Index(t => t.PlaceScheduleID)
                .Index(t => t.Schedule_ScheduleID);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.TransferOpenTickets",
                c => new
                    {
                        TransferOpenTicketID = c.Int(nullable: false, identity: true),
                        OpenTicketID = c.Int(nullable: false),
                        ApplicationUserID = c.String(nullable: false, maxLength: 128),
                        TransferDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TransferOpenTicketID)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserID)
                .ForeignKey("dbo.OpenTickets", t => t.OpenTicketID)
                .Index(t => t.OpenTicketID)
                .Index(t => t.ApplicationUserID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.TypeOpenTickets",
                c => new
                    {
                        TypeOpenTicketID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Length = c.Int(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDelete = c.Boolean(nullable: false),
                        DateStart = c.DateTime(nullable: false),
                        DateEnd = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TypeOpenTicketID);
            
            CreateTable(
                "dbo.TagFilms",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Film_FilmID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Film_FilmID })
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.Film_FilmID, cascadeDelete: true)
                .Index(t => t.Tag_TagID)
                .Index(t => t.Film_FilmID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ActiveOpenTickets", "ActiveOpenTicketID", "dbo.Tickets");
            DropForeignKey("dbo.ActiveOpenTickets", "ActiveOpenTicketID", "dbo.OpenTickets");
            DropForeignKey("dbo.OpenTickets", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.TransferOpenTickets", "OpenTicketID", "dbo.OpenTickets");
            DropForeignKey("dbo.TransferOpenTickets", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ReserveTickets", "PlaceScheduleID", "dbo.PlaceSchedules");
            DropForeignKey("dbo.PlaceSchedules", "ScheduleID", "dbo.Schedules");
            DropForeignKey("dbo.Tickets", "Schedule_ScheduleID", "dbo.Schedules");
            DropForeignKey("dbo.Tickets", "PlaceScheduleID", "dbo.PlaceSchedules");
            DropForeignKey("dbo.Tickets", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Schedules", "HallID", "dbo.Halls");
            DropForeignKey("dbo.Halls", "CinemaID", "dbo.Cinemas");
            DropForeignKey("dbo.Schedules", "FilmID", "dbo.Films");
            DropForeignKey("dbo.Tags", "TypeTagID", "dbo.TypeTags");
            DropForeignKey("dbo.TagFilms", "Film_FilmID", "dbo.Films");
            DropForeignKey("dbo.TagFilms", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.Reviews", "FilmID", "dbo.Films");
            DropForeignKey("dbo.Raitings", "ResourceID", "dbo.Resources");
            DropForeignKey("dbo.Raitings", "FilmID", "dbo.Films");
            DropForeignKey("dbo.ImageFilms", "FilmID", "dbo.Films");
            DropForeignKey("dbo.Films", "AgeLimitID", "dbo.AgeLimits");
            DropForeignKey("dbo.ReserveTickets", "ApplicationUserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.TagFilms", new[] { "Film_FilmID" });
            DropIndex("dbo.TagFilms", new[] { "Tag_TagID" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.TransferOpenTickets", new[] { "ApplicationUserID" });
            DropIndex("dbo.TransferOpenTickets", new[] { "OpenTicketID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.Tickets", new[] { "Schedule_ScheduleID" });
            DropIndex("dbo.Tickets", new[] { "PlaceScheduleID" });
            DropIndex("dbo.Tickets", new[] { "ApplicationUserID" });
            DropIndex("dbo.Halls", new[] { "CinemaID" });
            DropIndex("dbo.Tags", new[] { "TypeTagID" });
            DropIndex("dbo.Reviews", new[] { "FilmID" });
            DropIndex("dbo.Raitings", new[] { "FilmID" });
            DropIndex("dbo.Raitings", new[] { "ResourceID" });
            DropIndex("dbo.ImageFilms", new[] { "FilmID" });
            DropIndex("dbo.Films", new[] { "AgeLimitID" });
            DropIndex("dbo.Schedules", new[] { "HallID" });
            DropIndex("dbo.Schedules", new[] { "FilmID" });
            DropIndex("dbo.PlaceSchedules", new[] { "ScheduleID" });
            DropIndex("dbo.ReserveTickets", new[] { "PlaceScheduleID" });
            DropIndex("dbo.ReserveTickets", new[] { "ApplicationUserID" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.OpenTickets", new[] { "ApplicationUserID" });
            DropIndex("dbo.ActiveOpenTickets", new[] { "ActiveOpenTicketID" });
            DropTable("dbo.TagFilms");
            DropTable("dbo.TypeOpenTickets");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.TransferOpenTickets");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Tickets");
            DropTable("dbo.Cinemas");
            DropTable("dbo.Halls");
            DropTable("dbo.TypeTags");
            DropTable("dbo.Tags");
            DropTable("dbo.Reviews");
            DropTable("dbo.Resources");
            DropTable("dbo.Raitings");
            DropTable("dbo.ImageFilms");
            DropTable("dbo.AgeLimits");
            DropTable("dbo.Films");
            DropTable("dbo.Schedules");
            DropTable("dbo.PlaceSchedules");
            DropTable("dbo.ReserveTickets");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.OpenTickets");
            DropTable("dbo.ActiveOpenTickets");
        }
    }
}
