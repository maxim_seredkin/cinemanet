﻿using System.Web.Optimization;

namespace cinema
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/cta").Include(
    "~/Scripts/cta.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css/common").Include("~/Content/bootstrap.css",
                "~/Content/Common.css"));

            bundles.Add(new StyleBundle("~/Content/css/home").Include("~/Content/Home.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/Content/css/film").Include("~/Content/Film.css", new CssRewriteUrlTransform()));
        }
    }
}