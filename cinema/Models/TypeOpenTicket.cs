﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;


namespace cinema.Models
{
    public class TypeOpenTicket
    {
        public TypeOpenTicket()
        { }
        [Key]
        [Required]
        public int TypeOpenTicketID { get; set; }
        [Required]
        [Display(Name = "Название")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Длительность")]
        public int Length { get; set; }
        [Required]
        [Display(Name = "Стоимость")]
        public decimal Cost { get; set; }
        public bool IsDelete { get; set; }
        [Required]
        [Display(Name = "Дата начала действия")]
        public DateTime DateStart { get; set; }
        [Required]
        [Display(Name = "Дата окончания действия")]
        public DateTime DateEnd { get; set; }
    }
}