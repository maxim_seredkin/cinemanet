﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;

namespace cinema.Models
{

    public partial class Film
    {
        public Film()
        {
            this.Schedule = new HashSet<Schedule>();
            this.Tag = new HashSet<Tag>();
            this.Review = new HashSet<Review>();
            this.PathImage = new HashSet<ImageFilm>();
            this.Raiting = new HashSet<Raiting>();
        }
        [Key]
        public int FilmID { get; set; }
        [DataType("nvarchar(MAX)"), DisplayName("Фильм")]
        public string FilmName { get; set; }

        [DataType("nvarchar(MAX)"), DisplayName("Страна")]
        public string Country { get; set; }

        [DataType(DataType.Text), DisplayName("Продолжительность")]
        public System.TimeSpan Length { get; set; }

        [DataType(DataType.Date), DisplayName("Дата релиза")]
        public System.DateTime ReleaseDate { get; set; }
        [DisplayName("Бюджет")]
        public decimal Finance { get; set; }
        [DisplayName("Ссылка на трейлер")]
        public string UrlTrailer { get; set; }
        [DataType("nvarchar(MAX)"), DisplayName("Описание")]
        public string Description { get; set; }
        public int AgeLimitID { get; set; }
        public virtual AgeLimit AgeLimit { get; set; }
        public virtual ICollection<Schedule> Schedule { get; set; }
        public virtual ICollection<Tag> Tag { get; set; }
        public virtual ICollection<Review> Review { get; set; }
        public virtual ICollection<ImageFilm> PathImage { get; set; }
        public virtual ICollection<Raiting> Raiting { get; set; }
    }
    public class FilmServiceImpl
    {
        public IEnumerable<string> getFilmImage(int filmId, bool type)
        {
            //type == True - Poster
            //type == False - Thubnails
            //Не использовать
            return new ApplicationDbContext().ImageFilms.Where(row => row.FilmID == filmId && row.IsPoster == type).Select(row => row.FileName);
        }
        public IEnumerable<Raiting> getFilmRaiting(int filmId)
        {
            return new ApplicationDbContext().Raitings
                .Where(row => row.FilmID == filmId).Include(row => row.Resource);
        }
        public IEnumerable<Review> getFilmReviews(int filmId, int count)
        {
            List<Review> result = new ApplicationDbContext().Reviews.Where(row => row.FilmID == filmId).OrderBy(row => row.Date).ToList();
            return (result.Count() >= count)?result.GetRange(0, count):null;
        }
        public string getFilmProducer(int filmId)
        {
            IEnumerable<string> result = new ApplicationDbContext().Tags.Where(row => row.TypeTag.TypeTagID == 3 && row.Film.Where(film => film.FilmID == filmId).Count() > 0).Select(row => row.TagName);
            return result.Count() > 1 ? result.First() : null;
        }
        public string getFilmActors(int filmId)
        {
            string value = string.Empty;
            string str = ", ";
            //using (ApplicationDbContext)
            IEnumerable<string> collection = new ApplicationDbContext().Tags.Where(row => row.TypeTag.TypeTagID == 2
                && row.Film.Where(film => film.FilmID == filmId).Count() > 0).Select(row => row.TagName);
            foreach (string actor in collection)
            {
                value += actor + str;
            }
            return value.Substring(0, (value.Length - 2 > 1 )? value.Length - 2 :0);
        }
        public IEnumerable<Schedule> getFilmSchedule(DateTime startDay, int range, int filmId)
        {
            DateTime endDay = startDay.AddDays(range);
            return new ApplicationDbContext().Schedules.Where(row => row.FilmID == filmId && (row.ScheduleDate >= startDay && row.ScheduleDate < endDay))
                .Include(row => row.Hall.HallName).Include(row => row.Hall.Cinema.CinemaName)
                .OrderBy(row => row.Hall.Cinema.CinemaID).OrderBy(row => row.HallID).OrderBy(row => row.ScheduleDate);
        }
        public Film getFilmInfo(int filmId)
        {
            return new ApplicationDbContext().Films.Find(new object[] { filmId });
        }
        public IEnumerable<Film> findFilterAll(DateTime startDay, int cinemaId, int tagId, int limit)
        {
            DateTime endDay = startDay.AddDays(1);
            IEnumerable<Film> films = new ApplicationDbContext().Films
                                   .Where(film => film.Schedule.Where(schedule =>
                                   ((cinemaId == 0) || (schedule.Hall.Cinema.CinemaID == cinemaId))
                                   && (schedule.ScheduleDate >= startDay && schedule.ScheduleDate < endDay)).Count() > 0
                                   && ((tagId == 0) || (film.Tag.Where(tag => tag.TagID == tagId).Count() > 0))
                                   && film.AgeLimit.Age <= limit).Include("Schedule").Include("Tag").Include("AgeLimit");
            foreach (Film film in films)
            {
                film.Schedule = film.Schedule.Where(schedule => (schedule.ScheduleDate >= startDay && schedule.ScheduleDate < endDay))
                    .OrderBy(sc => sc.ScheduleDate.TimeOfDay).ToList();
            }
            return films;
        }
    }
}

