﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;

namespace cinema.Models
{

    public partial class Tag
    {
        public Tag()
        {
            this.Film = new HashSet<Film>();
            this.TypeTag = new TypeTag();
        }
        [Key]
        [Required]
        public int TagID { get; set; }
        [Required]
        [ForeignKey("TypeTag")]
        public int TypeTagID { get; set; }
        [Required]
        [Display(Name = "Имя")]
        public string TagName { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public virtual TypeTag TypeTag { get; set; }
        public virtual ICollection<Film> Film { get; set; }
    }
    public class TagServiceImpl
    {
        public IEnumerable<Tag> findAllGenre()
        {
            return new ApplicationDbContext().Tags.Where(row => row.TypeTagID == 1);
        }
    }
}
