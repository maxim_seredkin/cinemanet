﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace cinema.Models
{
    public class ImageFilm
    {
        public ImageFilm()
        {
            this.Film = new Film();
        }
        [Key]
        [Required]
        public int ImageFilmID { get; set; }
        [Required]
        [ForeignKey("Film")]
        public int FilmID { get; set; }
        [Required]
        public bool IsPoster { get; set; }
        [Required]
        public string FileName { get; set; }
        public virtual Film Film { get; set; }
    }
}