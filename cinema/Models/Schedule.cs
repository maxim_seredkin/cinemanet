﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity;

namespace cinema.Models
{

    public partial class Schedule
    {
        public Schedule()
        {
            this.Ticket = new HashSet<Ticket>();
        }
        [Key]
        [Required]
        public int ScheduleID { get; set; }
        [Required]
        [ForeignKey("Film")]
        public int FilmID { get; set; }
        [Required]
        [ForeignKey("Hall")]
        public int HallID { get; set; }
        [Required]
        [Display(Name = "Дата и время сеанса")]
        public System.DateTime ScheduleDate { get; set; }
        [Required]
        [Display(Name = "Стоимость")]
        public decimal TicketCost { get; set; }
        public bool? Is3D { get; set; }
        public virtual Film Film { get; set; }
        public virtual Hall Hall { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
    }

    public class DateServiceImpl
    {
        public IEnumerable<DateTime> findAll(int range)
        {
            DateTime[] Ie = new DateTime[range];
            for (int i = 0; i < range; i++)
            {
                Ie[i] = DateTime.Now.AddDays(i).Date;
            }
            return Ie;
        }
    }
}
