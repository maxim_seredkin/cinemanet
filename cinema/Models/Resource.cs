﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace cinema.Models
{
    public class Resource
    {
        public Resource()
        {
            this.Raiting = new HashSet<Raiting>();
        }
        [Key]
        [Required]
        public int ResourceID { get; set; }
        [Required]
        [DisplayName("Название")]
        public string ResourceName { get; set; }
        [Required]
        [DisplayName("Адрес ресурса")]
        public string Url { get; set; }
        [DisplayName("Описание")]
        public string Description { get; set; }
        public ICollection<Raiting> Raiting { get; set; }
    }
}