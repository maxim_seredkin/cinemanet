﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{

    public partial class OpenTicket
    {
        public OpenTicket()
        {
            this.ActiveOpenTicket = new ActiveOpenTicket();
            this.ApplicationUser = new ApplicationUser();
            this.TransferOpenTicket = new HashSet<TransferOpenTicket>();
        }
        [Key]
        [Required]
        public int OpenTicketID { get; set; }
        [Required]
        [ForeignKey("ApplicationUser")]
        public string ApplicationUserID { get; set; }
        [Required]
        [Display(Name = "Код билета")]
        public string CodeTicket { get; set; }
        [Required]
        [Display(Name = "Дата покупки")]
        public System.DateTime BuyDate { get; set; }
        [Required]
        [Display(Name = "Дата начала срока действия")]
        public System.DateTime StartDate { get; set; }
        [Required]
        [Display(Name = "Дата окончания срока действия")]
        public System.DateTime EndDate { get; set; }
        [Required]
        [Display(Name = "Стоимость")]
        public decimal Cost { get; set; }
        [Required]
        public bool IsActive { get; set; }
        public virtual ActiveOpenTicket ActiveOpenTicket { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<TransferOpenTicket> TransferOpenTicket { get; set; }
    }
}
