﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{
    public class PlaceSchedule
    {
        public PlaceSchedule()
        {
            this.Schedule = new Schedule();
            this.Ticket = new HashSet<Ticket>();
            this.ReserveTicket = new HashSet<ReserveTicket>();
        }
        [Key]
        [Required]
        public int PlaceScheduleID { get; set; }
        [Required]
        [ForeignKey("Schedule")]
        public int ScheduleID { get; set; }
        [Required]
        public int Row { get; set; }
        [Required]
        public int Place { get; set; }
        public virtual Schedule Schedule { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
        public virtual ICollection<ReserveTicket> ReserveTicket { get; set; }
    }
}