﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{
    public class ReserveTicket
    {
        public ReserveTicket()
        {
            this.ApplicationUser = new ApplicationUser();
            this.PlaceSchedule = new PlaceSchedule();
        }
        [Key]
        [Required]
        public int ReserveTicketID { get; set; }
        [Required]
        [ForeignKey("ApplicationUser")]
        public string ApplicationUserID { get; set; }
        [Required]
        [ForeignKey("PlaceSchedule")]
        public int PlaceScheduleID { get; set; }
        [Required]
        [Display(Name = "Код билета")]
        public string CodeTicket { get; set; }
        [Required]
        [Display(Name = "Дата бронирования")]
        public DateTime ReserveDate { get; set; }
        [Required]
        [Display(Name = "Дата отмены бронирования")]
        public DateTime EndReserveDate { get; set; }
        public bool IsConfirmed { get; set; }
        public virtual PlaceSchedule PlaceSchedule { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}