﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{
    public partial class Ticket
    {
        public Ticket()
        {
            this.ActiveOpenTicket = new ActiveOpenTicket();
            this.ApplicationUser = new ApplicationUser();
            this.PlaceSchedule = new PlaceSchedule();
        }
        [Key]
        [Required]
        public int TicketID { get; set; }
        [Required]
        [ForeignKey("ApplicationUser")]
        public string ApplicationUserID { get; set; }
        [Required]
        public int PlaceScheduleID { get; set; }
        [Required]
        [Display(Name = "Код билета")]
        public string CodeTicket { get; set; }
        [Required]
        [Display(Name = "Дата покупки")]
        public System.DateTime BuyDate { get; set; }
        [Required]
        public bool IsOpen { get; set; }
        [Required]
        public bool IsReserve { get; set; }
        public virtual ActiveOpenTicket ActiveOpenTicket { get; set; }
        public virtual PlaceSchedule PlaceSchedule { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}