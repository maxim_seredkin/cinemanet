﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace cinema.Models
{
    public class Raiting
    {
        public Raiting()
        {
            this.Resource = new Resource();
            this.Film = new Film();
        }
        [Key]
        [Required]
        public int RaitingID { get; set; }
        [Required]
        [ForeignKey("Resource")]
        public int ResourceID { get; set; }
        [Required]
        [ForeignKey("Film")]
        public int FilmID { get; set; }
        [Required]
        [DisplayName("Рейтинг")]
        public double Value { get; set; }
        public virtual Resource Resource { get; set; }
        public virtual Film Film { get; set; }
    }
}