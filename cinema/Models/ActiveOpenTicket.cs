using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{
    
    public partial class ActiveOpenTicket
    {
        public ActiveOpenTicket()
        {
            this.OpenTicket = new OpenTicket();
            this.Ticket = new Ticket();
        }
        [Key]
        [Required]
        public int ActiveOpenTicketID { get; set; }
        [Required]
        public int OpenTicketID { get; set; }
        [Required]
        public int TicketID { get; set; }
        [Required]
        public virtual OpenTicket OpenTicket { get; set; }
        [Required]
        public virtual Ticket Ticket { get; set; }
    }
}
