﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace cinema.Models
{
    public sealed class AgeLimit
    {
        public AgeLimit() 
        {
            this.Film = new HashSet<Film>();
        }
        [Key]
        [Required]
        public int AgeLimitID { get; set; }
        [Required]
        [DisplayName(@"Возрастное ограничение")]
        public int Age { get; set; }
        [Required]
        [DisplayName(@"Описание")]
        public string Description { get; set; }
        public ICollection<Film> Film { get; set; }
    }
    public class AgeLimitServiceImpl
    {
        public IEnumerable<AgeLimit> FindAll()
        {
            return new ApplicationDbContext().AgeLimits;
        }
    } 
}