﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace cinema.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            OpenTicket = new HashSet<OpenTicket>();
            Ticket = new HashSet<Ticket>();
            ReserveTicket = new HashSet<ReserveTicket>();
            TransferOpenTicket = new HashSet<TransferOpenTicket>();
        }
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [Display(Name = "Дата рождения")]
        public DateTime? DateBorn { get; set; }
        public virtual ICollection<OpenTicket> OpenTicket { get; set; }
        public virtual ICollection<Ticket> Ticket { get; set; }
        public virtual ICollection<ReserveTicket> ReserveTicket { get; set; }
        public virtual ICollection<TransferOpenTicket> TransferOpenTicket { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ActiveOpenTicket> ActiveOpenTickets { get; set; }
        public DbSet<AgeLimit> AgeLimits { get; set; }
        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Hall> Halls { get; set; }
        public DbSet<ImageFilm> ImageFilms { get; set; }
        public DbSet<OpenTicket> OpenTickets { get; set; }
        public DbSet<PlaceSchedule> PlaceSchedules { get; set; }
        public DbSet<Raiting> Raitings { get; set; }
        public DbSet<ReserveTicket> ReserveTickets { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TransferOpenTicket> TransferOpenTickets { get; set; }
        public DbSet<TypeOpenTicket> TypeOpenTickets { get; set; }
        public DbSet<TypeTag> TypeTags { get; set; }
        public ApplicationDbContext() : base("DefaultConnection", throwIfV1Schema: false) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}