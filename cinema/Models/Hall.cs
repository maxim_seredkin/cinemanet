﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{
    
    public partial class Hall
    {
        public Hall()
        {
            this.Schedule = new HashSet<Schedule>();
        }
        [Key]
        [Required]
        public int HallID { get; set; }
        [Required]
        [Display(Name="Зала")]
        public string HallName { get; set; }
        [Required]
        [Display(Name="Ряды")]
        public int Row { get; set; }
        [Required]
        [Display(Name="Места")]
        public int Place { get; set; }
        [Display(Name="Ширина экрана")]
        public int? ScreenWidth { get; set; }
        [Display(Name = "Высота экрана")]
        public int? ScreenHeigth { get; set; }

        [Required]
        [ForeignKey("Cinema")]
        public int CinemaID { get; set; }
        public virtual Cinema Cinema { get; set; }
        public virtual ICollection<Schedule> Schedule { get; set; }
    }
}
