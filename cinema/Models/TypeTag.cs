﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace cinema.Models
{
    public class TypeTag
    {
        public TypeTag()
        {
            this.Tag = new HashSet<Tag>();
        }
        [Key]
        [Required]
        public int TypeTagID { get; set; }
        [Required]
        [DisplayName("Название")]
        public string Name { get; set; }
        public virtual ICollection<Tag> Tag { get; set; }
    }
}