﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace cinema.Models
{
    public class Review
    {
        public Review()
        {
            this.Film = new Film();
        }
        [Key]
        [Required]
        public int ReviewID { get; set; }
        [Required]
        [ForeignKey("Film")]
        public int FilmID { get; set; }
        [Required]
        [DisplayName("Качество рецензии")]
        public bool IsLike { get; set; }
        [Required]
        [DisplayName("Дата опубликования")]
        public DateTime Date { get; set; }
        [Required]
        [DisplayName("Название")]
        public string Title { get; set; }
        [Required]
        [DisplayName("Описание")]
        public string Description { get; set; }
        public virtual Film Film { get; set; }
    }
}