﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Linq;

namespace cinema.Models
{
    public partial class Cinema
    {
        public Cinema()
        {
            this.Hall = new HashSet<Hall>();
        }
        [Key]
        public int CinemaID { get; set; }
        [Required]
        [DisplayName("Адрес")]
        public string Address { get; set; }
        [Required]
        [DisplayName("Кинотеатр")]
        public string CinemaName { get; set; }
        public virtual ICollection<Hall> Hall { get; set; }
    }
    public class CinemaServiceImpl
    {
        public IEnumerable<Cinema> findAll()
        {
            return new ApplicationDbContext().Cinemas;
        }
    }
}
