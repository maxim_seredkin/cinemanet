﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace cinema.Models
{
    public class TransferOpenTicket
    {
        public TransferOpenTicket()
        {
            this.ApplicationUser = new ApplicationUser();
            this.OpenTicket = new OpenTicket();
        }
        [Key]
        [Required]
        public int TransferOpenTicketID { get; set; }
        [Required]
        [ForeignKey("OpenTicket")]
        public int OpenTicketID { get; set; }
        [Required]
        [ForeignKey("ApplicationUser")]
        public string ApplicationUserID { get; set; }
        [Required]
        public DateTime TransferDate { get; set; }
        public virtual OpenTicket OpenTicket { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}